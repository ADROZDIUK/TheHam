const M = {
    mainMasonry: 'grid',
    loadMore: 'section-eight_loadmore',
    waitLoadImgMasonry: 'section-eight_imgblock-load',
    img: 'masonry_img',
    imgHover: 'section-eight_grid-hover',
    sizerMasonary: 'fa-arrows-alt',
    imgCloser: 'fa-sistrix',

}
let indexImg = 0;
let numLoad = 12;
let arrMasonry = [];

$(document).ready(function(){
    // masonry init
    let grid = $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: 1,
    }); 

    const runRandomSize = () => {
        const arrSize = ["grid-item", "grid-item grid-item--height2", "grid-item grid-item--width2","grid-item grid-item--width3", "grid-item grid-item--height3", "grid-item grid-item--width2 grid-item--height2", "grid-item grid-item--width2 grid-item--height3", "grid-item grid-item--width3 grid-item--height2"];
       let arrSizeItem = Math.floor(Math.random() * arrSize.length);
       return arrSize[arrSizeItem];
    }

    const loadMasonryImg = () => {
        $(`#${M.waitLoadImgMasonry}`).removeClass(M.waitLoadImgMasonry); //hide animation load here
       
        if(indexImg != numLoad) {
            indexImg++;
            let attrSrcR = String("img/bestImg/" + indexImg + ".jpg");
            let masonryitems =  $("<div/>", {class:runRandomSize()});

            $("<img/>").attr({src: attrSrcR, class:"masonry_img"}).appendTo(masonryitems); //add masonary child 1
            $(`<div class="section-eight_grid-hover">         
                    <span class="fab fa-sistrix"></span>
                    <span class="fas fa-arrows-alt"></span>
                </div>`).appendTo(masonryitems).hide(); //add masonary child 2

            grid.imagesLoaded( function() {  //wait load file and init masonry
                grid.append(masonryitems).masonry('appended', masonryitems );
                loadMasonryImg();
            });          
        };
        if(numLoad >= 36){ 
            $(`.${M.loadMore}`).hide()
        };
        //if mouse over to img in block 5
        $(`.${M.mainMasonry}>div`).mouseover(function() {
            $(this).children(`.${M.imgHover}`).show();

        });
        //if mouse out to img in block 5
        $(`.${M.mainMasonry}>div`).mouseout(function() {
            $(this).children(`.${M.imgHover}`).hide();
        });
    };

    $(`.${M.loadMore}`).on("click", function() {
        $(`#${M.waitLoadImgMasonry}`).addClass(M.waitLoadImgMasonry);  //on animation here
        numLoad += 12;
        function remAnimClassMasonry() {
            loadMasonryImg();
        }
        setTimeout(remAnimClassMasonry, 2000);
    });
    
    grid.on('click', `.${M.sizerMasonary}`, function() {
        $(event.target).parent().parent().toggleClass('grid-item--mini');
        grid.masonry();
    });

    grid.on('click', `.${M.imgCloser}`, function() {
        $(event.target).parent().parent().toggleClass('grid-item--closer');
        $(event.target).parent().hide();
        grid.masonry();
    });

    loadMasonryImg(); //1st load 12 photo
});

