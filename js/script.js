const S = {
	switcherChild: 'section-three_switchblock div',
	switcherCheck: 'switcher_check',
	switchConteinerChild: 'section-three_switchconteiner>div',
	showContent: 'section-three_switchblock-content',
}

const C = {
	switcherChild: 'section-five_switchblock div',
	switcherCheck: 'switcher2_check',
	imgblock: 'section-five_imgblock-active',
	loadImg5: 'section-five_imgblock-loadmore',
	waitLoadImg5: 'section-five_imgblock-load',
	hoveredImg: 'section-five_img-hovered',
	motherBlock: 'imgblock-active_mather',
}

const H = {
	activePhoto: 'talkinghead_active-photo',
	activeJob: 'talkinghead_active-job',
	activeName: 'talkinghead_active-name',
	activeTittleText: "talkinghead_tittle-text",
	leftClick: 'carousel_buttonleft',
	rightClick: 'carousel_buttonright',
	carouselBlock: 'section-seven_carousel',
	carouselBlockImg: 'carousel_imgpanel',
	imgActiveNow: 'img_active',
	talkingHead: 'section-seven_talkinghead',
}

const people = [{
	name:"Alisa Milano",
	job: "Cleaner",
	photo:"img/talkinghead/1.png",
	comment: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime voluptates commodi aspernatur aliquam quasi! Eveniet, ad? Natus nulla quaerat fugiat in, assumenda unde placeat dolor, commodi ab omnis saepe officiis!",
},{
	name:"John Bravo",
	job: "Actor",
	photo:"img/talkinghead/2.png",
	comment: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime voluptates commodi aspernatur  voluptates commodi aspernatur aliquam quasi! Eveniet, ad? Natus nulla quaerat fugiat in, assumenda unde placeat dolor, commodi ab omnis saepe officiis! It's!!!",
},{
	name:"Husan Ali",
	job: "UX Designer",
	photo:"img/talkinghead/3.png",
	comment: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Maxime voluptates commodi aspernatur aliquam quasi! Eveniet, ad? Natus nulla quaerat fugiat in, assumenda unde placeat dolor, amet consectetur adipisicing elit. Maxime voluptates commodi aspernatur ",
},{
	name:"Maria Semual",
	job: "QA",
	photo:"img/talkinghead/4.png",
	comment: "Not Lorem ipsum dolor sit, but, amet consectetur adipisicing elit. Maxime voluptates commodi aspernatur aliquam quasi! Eveniet, ad? Natus nulla quaerat fugiat in, assumenda unde placeat dolor, commodi ab omnis saepe officiis!amet consectetur adipisicing elit, aspernatur ",
}];
// variable end(in top list)
$(document).ready(function(){
  $(`.${S.switcherChild}`).click(function() {     //Our Services block - switcher
	$(`.${S.switcherChild}`).removeClass(S.switcherCheck);         
	$(this).toggleClass(S.switcherCheck);
	let clikedIndex = $(this).index();
	$(`.${S.switchConteinerChild}`).removeClass();
	$(`.${S.switchConteinerChild}`).eq(clikedIndex).addClass(S.showContent);
  })
  
  $(`.${C.switcherChild}`).click(function() {     //Our Amazing Work block - switcher
	$(`.${C.switcherChild}`).removeClass(C.switcherCheck);         
	$(this).toggleClass(C.switcherCheck);
	switchDerictoryFive(this);
  })
//function load  photo in section-five_imgblock-active
let dir5 = "";
const fileextension = ".jpg"; 
let i = 0;
let n = 12;
let caseDir5 = 0;

const switchDerictoryFive = (elToSwitchImg) => { 
	let indexOfChildC = $(`.${C.switcherChild}`).index(elToSwitchImg);
	switch(indexOfChildC){ //change directory if click switcher - block 5
	  case 0: 
		caseDir5 = 0;
		break;
	  case 1: 
		dir5 = "img/graphic_design/";
		caseDir5 = 1;
		break;
	  case 2: 
		dir5 = "img/web_design/";
		caseDir5 = 1;
		break;
	  case 3: 
		dir5 = "img/landing_page/";
		caseDir5 = 1;
		break;
	  case 4: 
		dir5 = "img/wordpress/";
		caseDir5 = 1;
		break;
	}
	i = 0;
	n = 12;
	$(`.${C.imgblock}`).empty();
	$(`.${C.loadImg5}`).show();
	imageloop(caseDir5);
}
  
const randomImgDir5 = () => {
	const arrDir5 = ["img/graphic_design/", "img/web_design/", "img/landing_page/", "img/wordpress/"];
	return arrDir5[Math.floor(Math.random() * arrDir5.length)];
};
//find name of derictory in hide block in C.motherBlock
let nameOfDirectory = "";
const whatDirectory = (dirName) => {
	if(dirName === "img/graphic_design/"){
		return nameOfDirectory = "Graphic design";
	} else if(dirName === "img/web_design/"){
		return nameOfDirectory = "Web design";
	}else if(dirName === "img/landing_page/"){
		return nameOfDirectory = "Lending Pages";
	}else if(dirName === "img/wordpress/"){
		return nameOfDirectory = "Word Press";
	}
};
	
$(`.${C.loadImg5}`).click(function() { //add new 12 photo
	$(`#${C.waitLoadImg5}`).addClass(C.waitLoadImg5);
	n = n + 12;
	function remAnimClass() {
		imageloop(caseDir5);
	  }
	setTimeout(remAnimClass, 2000);
});

const imageloop = (direction) => {
	$(`#${C.waitLoadImg5}`).removeClass(C.waitLoadImg5);
	if(direction === 0 && i != n){  
		i++;
		const hoverImgRan = $("<div/>", {class:`imgblock-active_mather`});
			$(hoverImgRan).appendTo(`.${C.imgblock}`);
			let attrSrcR = String(randomImgDir5() + i + fileextension);
			$("<img/>").attr({src: attrSrcR, class:"section-five_img"}).appendTo(hoverImgRan);
			let iRandomSlice = attrSrcR.length - attrSrcR.lastIndexOf("/")-1;
			whatDirectory(attrSrcR.slice(0,-iRandomSlice));
			$(`<div class="section-five_img-hovered">
					<div class="five_img-hovered_link">
						<a href="#" class="fas fa-link"></a>
						<a href="#" class="fab fa-sistrix"></a>
					</div>
					<div class="five_img-hovered-text">
						<span class="five_hovered-text-header">Creative Design</span>
						<span id="five_hovered-text-tittle">${nameOfDirectory}</span>
					</div>
				</div>`).appendTo(hoverImgRan).hide();
		imageloop(direction);

	} else if (i != n){
		i++;
		whatDirectory(dir5);
		const hoverImgNotRan = $("<div/>", {class:`imgblock-active_mather`});
			$(hoverImgNotRan).appendTo(`.${C.imgblock}`);
			let atrSrc = String(dir5 + i + fileextension);
			$("<img/>").attr({src: atrSrc, class:"section-five_img"}).appendTo(hoverImgNotRan);
			$(`<div class="section-five_img-hovered">
					<div class="five_img-hovered_link">
						<a href="#" class="fas fa-link"></a>
						<a href="#" class="fab fa-sistrix"></a>
					</div>
					<div class="five_img-hovered-text">
						<span class="five_hovered-text-header">Creative Design</span>
						<span id="five_hovered-text-tittle">${nameOfDirectory}</span>
					</div>
				</div>`).appendTo(hoverImgNotRan).hide();
		imageloop(direction);

	}else if(i === 36){   //if img in this block 36 - hide button "load more"
		$(`.${C.loadImg5}`).hide()
	}
	//if mouse over to img in block 5
	$(`.${C.imgblock}>div`).mouseover(function() {
		$(this).children(".section-five_img").hide();
		$(this).children(".section-five_img-hovered").show();
		
	});
	//if mouse out to img in block 5
	$(`.${C.imgblock}>div`).mouseout(function() {
		$(this).children(".section-five_img-hovered").hide();
		$(this).children(".section-five_img").show();
	});
};
imageloop(caseDir5); // first render img in 5 block - start load
//start script block 7(Client feedback)
let changeImgInx = Number($(`.${H.imgActiveNow}`).index());
const showImgCarousel  = (index) => {
	$(`#${H.carouselBlockImg} div`).removeClass("miniphoto-up");
	$(`#${H.carouselBlockImg} img`).removeClass("img_active");
	$(`#${H.carouselBlockImg} img`).eq(index).addClass("img_active");
	$(`#${H.carouselBlockImg} div`).eq(index).addClass("miniphoto-up");
	$(`#${H.activeTittleText}`).text(people[index].comment);
	$(`#${H.activeJob}`).text(people[index].job);
	$(`#${H.activeName}`).text(people[index].name);
	$(`#${H.activePhoto}`).attr("src",people[index].photo);  //need new photo
	function remAnimClass() {
		$(`#${H.talkingHead}`).removeClass('runanamation-sevenblock-left');
		$(`#${H.talkingHead}`).removeClass('runanamation-sevenblock-right');
		$(`#${H.talkingHead}`).removeClass('runanamation-sevenblock-down');
	  }
	setTimeout(remAnimClass, 550);
	
return changeImgInx = index;
};

	$(`#${H.rightClick}`).on("click", function () {  //if right click
		$(`#${H.talkingHead}`).addClass('runanamation-sevenblock-left');
		if(changeImgInx === 3){
			showImgCarousel(0);
		} else {showImgCarousel(changeImgInx+ 1)}	
	});

	$(`#${H.leftClick}`).on("click", function () {  //if left click
		$(`#${H.talkingHead}`).addClass('runanamation-sevenblock-right');
		if(changeImgInx === 0){
			showImgCarousel(3);
		} else {showImgCarousel(changeImgInx - 1)}
	});

	$(`#${H.carouselBlockImg}>div`).on("click", function () { //if click icon img
		$(`#${H.talkingHead}`).addClass('runanamation-sevenblock-down');
		$(`#${H.carouselBlockImg} img`).removeClass();
		showImgCarousel($(this).index()); //we have index here 0-3
	});
	//add button scroll up (return void)	
	$(window).scroll(function(){
		if ($(this).scrollTop() > 300) {
			$('.scrollup').fadeIn();
		} else {
			$('.scrollup').fadeOut();
		}
	});
	$('.scrollup').click(function(){
	$("html, body").animate({ scrollTop: 0 }, 600);
		return false;
	});

});
